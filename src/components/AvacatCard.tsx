import React, { RefObject } from "react";
import Accordion from "./Accordion";
interface RefProps {
  ref1: RefObject<HTMLDivElement>;
  ref2: RefObject<HTMLDivElement>;
  ref3: RefObject<HTMLDivElement>;
}
const AvacatCard = ({ ref2, ref1, ref3 }: RefProps) => {
  const scrollToSection = (ref: RefObject<HTMLDivElement>) => {
    ref.current?.scrollIntoView({ behavior: "smooth" });
  };
  return (
    <div className=" flex flex-col items-center justify-center">
      <div
        ref={ref1}
        className=" flex flex-col items-center justify-center relative bg-bg1 bg-center bg-cover w-full h-[714px]"
      >
        <div className=" bg-[#262626] opacity-50 h-full w-full"></div>
        <div className=" text-white text-center opacity-100 z-10 absolute">
          <h3 className="text-primary uppercase text-3xl">MAÎTRE,</h3>
          <h2 className="text-5xl font-bold capitalize my-10 ">
            GUY BERTRAND DJIAMO
          </h2>
          <p className="text-xl">Avocat au Barreau du Québec, canada</p>
          <button
            onClick={() => scrollToSection(ref3)}
            className=" px-2 md:w-[400px] text-sm md:text-base mt-28 py-3 bg-primary"
          >
            CONTACTEZ-MOI MAINTENANT{" "}
          </button>
        </div>
      </div>
      <div className="px-5 lg:px-[100px] flex items-center flex-col 2xl:px-[266px] relative w-full top-[-70px]">
        <div className="bg-white rounded-lg shadow-md w-full lg:min-w-[984px] xl:min-w-[1133px] lg:h-[400px] flex flex-col lg:flex-row">
          <div className=" p-3 md:p-10 w-full border rounded-t-md lg:rounded-ss-md lg:w-[50%] text-brun ">
            <h2 className=" font-bold">PRÉSENTATION</h2>
            <h3 className="my-[20px]">
              Avocat au barreau du Québec depuis 2018
            </h3>
            <ul className="">
              {[
                "Membre en règle du barreau de Québec.",
                "Membre en règle de l'association des avocats de la défense de Québec.",
                "Membre en règle du Jeune barreau de Québec.",
                "Membre du service de référence barreau de Québec.",
              ].map((item, index) => (
                <li className=" flex items-center gap-2">
                  <img src="/assets/arrow-right-outline.svg" alt="" />
                  <p className="py-2">{item}</p>
                </li>
              ))}
            </ul>
            <p
              style={{ fontFamily: "Bresley" }}
              className="mt-3 text-[#8E8E8E] text-6xl"
            >
              Contactez-moi
            </p>
          </div>
          <div className="h-[300px] md:h-[500px] bg-bg3 rounded-b-md bg-center bg-no-repeat bg-cover  w-full lg:h-[400px] lg:w-[50%]  lg:rounded-r-md "></div>
        </div>
        <div ref={ref2} className=" w-full lg:min-w-[984px] xl:min-w-[1133px] ">
          <div className=" flex flex-col items-center mt-10 justify-center">
            <hr className=" h-10 mb-8 bg-primary w-[2px]" />
            <h2 className="  font-bold text-4xl">Compétences</h2>
          </div>
          <div className=" flex lg:flex-row flex-col  gap-5 mt-10">
            <div className=" w-full lg:w-1/2 flex flex-col gap-y-5">
              {[
                {
                  droit: "Droit civil",
                  icon: "/assets/Droit civil.svg",
                },
                {
                  droit: "Immigration",
                  icon: "/assets/Immigration.svg",
                },
                {
                  droit: "Droit du travail",
                  icon: "/assets/Droit du travail.svg",
                },
                {
                  droit: "Droit Pénal et criminel",
                  icon: "/assets/Droit Pénal et criminel.svg",
                },
              ].map((item) => (
                <Accordion TypeDroit={item} />
              ))}
            </div>
            <div className=" w-full lg:w-1/2 flex flex-col gap-y-5">
              {[
                {
                  droit: "Droit de la famille",
                  icon: "/assets/Droit de la famille.svg",
                },
                {
                  droit: "Droit des affaires",
                  icon: "/assets/Droit des affaires.svg",
                },
                {
                  droit: "Droit internationnal",
                  icon: "/assets/Droit internationnal.svg",
                },
                {
                  droit: "Droit des assurances",
                  icon: "/assets/Droit des assurances.svg",
                },
              ].map((item) => (
                <Accordion TypeDroit={item} />
              ))}
            </div>
          </div>

          <div className=" flex items-center mt-[141px] justify-center">
            <p className="text-center text-[#5D5D5D]">
              Pour une consultation rapide, pour de plus amples information ou
              pour planifier un moment de <br /> consultation en personne ou à
              distance. veuillez remplir le formulaire ci-dessous. <br />{" "}
              Veuillez également nous contacter par téléphone ou par courriel
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AvacatCard;
