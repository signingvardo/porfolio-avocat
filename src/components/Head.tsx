import React, { useState, RefObject } from "react";
interface HeadProps {
  ref1: RefObject<HTMLDivElement>;
  ref2: RefObject<HTMLDivElement>;
  ref3: RefObject<HTMLDivElement>;
}
const Head = ({ ref1, ref2, ref3 }: HeadProps) => {
  const [select, setSelect] = useState(0);
  const scrollToSection = (ref: RefObject<HTMLDivElement>) => {
    ref.current?.scrollIntoView({ behavior: "smooth" });
  };
  const menu = [
    { titre: "Présentation", ref: ref1 },
    { titre: "Compétences", ref: ref2 },
    { titre: "Contactez-moi", ref: ref3 },
  ];
  return (
    <div>
      <div className="pl-5 lg:px-[100px] 2xl:px-[266px] md:h-[66px] shadow-md flex items-center justify-between ">
        <div className=" lg:w-[400px] lg:flex lg:justify-between gap-x-5 text-xs md:text-base text-secondary">
          <div className=" cursor-pointer flex space-x-2 items-center">
            <img src="/assets/Call.svg" alt="" />
            <p className="">+1-418-914-1613</p>
          </div>
          <div className=" cursor-pointer mt-1 lg:mt-0 flex space-x-2 items-center">
            <img src="/assets/mail-open-outline.svg" alt="" />
            <p>gbdjiamo@djiamoavocat.ca</p>
          </div>
        </div>
        <div className="flex items-center">
          <div className=" flex items-center justify-between">
            <img
              className=" cursor-pointer "
              src="/assets/Facebook Icon.svg"
              alt=""
            />
            <img
              src="/assets/LinkedIn Icon.svg"
              className="ml-5 cursor-pointer "
              alt=""
            />
          </div>
          <button
            onClick={() => {
              scrollToSection(ref3);
            }}
            className="flex items-center md:w-[225px] px-1 ml-4 md:ml-8 justify-center h-[66px]  text-white bg-primary "
          >
            <img
              src="/assets/Icon Message.svg"
              alt=""
              className=" mr-1 md:mr-2"
            />
            <p className="text-xs md:text-base">CONSULTATION</p>
          </button>
        </div>
      </div>
      <div className="h-[150px] px-5 lg:px-[100px] 2xl:px-[266px] flex flex-col lg:flex-row items-center justify-center gap-5 lg:gap-0 lg:justify-between ">
        <p className=" font-bold">ME GUY BERTRAND DJIOMO</p>
        <nav className=" w-full md:w-[400px]">
          <ul className=" flex justify-between text-xs md:text-base">
            {menu.map((item, i) => (
              <div className=" flex flex-col items-center">
                <li
                  onClick={() => {
                    setSelect(i);
                    scrollToSection(item.ref);
                  }}
                  className={`${
                    select === i ? "text-primary" : ""
                  } cursor-pointer`}
                  key={i}
                >
                  {item.titre}
                </li>
                <hr
                  className={`${
                    i === select ? "flex" : "hidden"
                  } bg-primary h-5 w-[1px]`}
                />
              </div>
            ))}
          </ul>
        </nav>
      </div>
    </div>
  );
};

export default Head;
