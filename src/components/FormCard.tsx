import React, { RefObject, useEffect, useState } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";

interface RefProps {
  ref3: RefObject<HTMLDivElement>;
}
const FormCard = ({ ref3 }: RefProps) => {
  const [select, setSelect] = useState(false);
  const [value, setValue] = useState("");
  const formik = useFormik({
    initialValues: {
      domaine: value,
      nom: "",
      telephone: "",
      adresse: "",
      entreprise: "",
      description: "",
    },
    validationSchema: Yup.object({
      domaine: Yup.string().required("Ce champs est requis"),
      description: Yup.string().required("Ce champs est requis"),
      nom: Yup.string().required("Ce champs est requis"),
      adresse: Yup.string().email().required("Ce champs est requis"),
      telephone: Yup.number().required("Ce champs est requis"),
      entreprise: Yup.string().required("Ce champs est requis"),
    }),
    onSubmit: () => {},
  });

  useEffect(() => {
    const input = document.querySelector("#custom_selected_input");
    input?.addEventListener("focusout", (e) => {
      setTimeout(() => {
        setSelect(false);
      }, 200);
    });
  }, []);
  const handleChange = (valueInput: any) => {
    setValue(valueInput);
    setTimeout(() => {
      setSelect(false);
    }, 200);
  };

  return (
    <div
      ref={ref3}
      className=" bg-bg2 bg-cover  relative  w-full min-h-[1183px] h-full"
    >
      <div className=" bg-[#262626b7] min-h-[1183px] h-full w-full">
        <div className="px-5 pt-[60px]  z-10 ">
          <div className=" shadow-md pt-[20px] pb-[60px]  w-full lg:w-[800px] rounded-md overflow-hidden bg-white m-auto">
            <div className="flex flex-col relative items-center justify-center">
              <div className="flex pt-[36px] flex-col z-10 items-center justify-center">
                <h2 className=" font-bold text-4xl">Consultation</h2>
                <p className=" mt-5 text-center">
                  Le droit est une affaire souvent complexe.Cela peut vous{" "}
                  <br /> causer de gros problèmes si vous l'ignorez. <br />{" "}
                  Laissez moi vous aider!{" "}
                </p>
              </div>
              <img
                height={"200px"}
                className=" absolute top-[-24px] right-[0px] md:w-[400px] opacity-20"
                width={"200px"}
                src="/assets/balance"
                alt=""
              />
            </div>
            <form
              onSubmit={formik.handleSubmit}
              className=" pt-14 md:mx-5 px-5 lg:px-[100px] relative z-50"
            >
              <div className=" relative">
                <input
                  onFocus={(e) => {
                    console.log("les events", e);
                    setSelect(true);
                  }}
                  id="custom_selected_input"
                  name="domaine"
                  className=" outline-none w-full border-[#707070] border h-[71px]  bg-input py-4 pl-3 "
                  placeholder="Domaine d'expertise *"
                  value={value}
                />
                <img
                  className={`${
                    select ? "rotate-180 transition-all" : "transition-all"
                  } absolute right-3 cursor-pointer top-[30px]`}
                  onClick={() => setSelect(!select)}
                  src="/assets/Chevron.svg"
                  alt=""
                />
                {select && (
                  <div
                    className={`bg-input border-[#707070] border py-5 absolute z-10 w-full`}
                  >
                    {[
                      "Droit civil",
                      "Immigration",
                      "Droit du travail",
                      "Droit Pénal et criminel",
                      "Droit de la famille",
                      "Droit des affaires",
                      "Droit internationnal",
                      "Droit des assurances",
                    ].map((item) => (
                      <p
                        className={`${
                          item === value ? "bg-primary text-white" : ""
                        } cursor-pointer  hover:bg-primary  px-3 flex items-center h-8 hover:text-white`}
                        onClick={(e) => {
                          handleChange(item);
                          e.stopPropagation();
                        }}
                      >
                        {item}
                      </p>
                    ))}
                  </div>
                )}
              </div>
              <div className=" mt-5 grid grid-cols-1 lg:grid-cols-2 gap-5 justify-between">
                <div className="flex flex-col">
                  <input
                    name="nom"
                    className={`${
                      formik.touched.nom && formik.errors.nom
                        ? "border-2 border-red-600"
                        : null
                    } outline-none  bg-input h-[71px]  py-4 pl-3 placeholder:text-sm`}
                    placeholder="Votre Nom *"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.nom}
                    type="text"
                  />
                </div>
                <div className="flex flex-col">
                  <input
                    name="telephone"
                    type="text"
                    className={`${
                      formik.touched.telephone && formik.errors.telephone
                        ? "border-2 border-red-600"
                        : null
                    } outline-none  bg-input h-[71px]  py-4 pl-3  placeholder:text-sm`}
                    placeholder="Votre téléphone *"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.telephone}
                  />
                </div>
                <div className="flex flex-col">
                  <input
                    type="email"
                    name="adresse"
                    className={`${
                      formik.touched.adresse && formik.errors.adresse
                        ? "border-2 border-red-600"
                        : null
                    } outline-none  bg-input h-[71px]  py-4 pl-3 placeholder:text-sm`}
                    placeholder="Votre adresse Email*"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.adresse}
                  />
                </div>
                <div className="flex flex-col">
                  <input
                    name="entreprise"
                    type="text"
                    className={`${
                      formik.touched.entreprise && formik.errors.entreprise
                        ? "border-2 border-red-600"
                        : null
                    } outline-none  bg-input h-[71px]  py-4 pl-3 placeholder:text-sm`}
                    placeholder="Votre entreprise *"
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    value={formik.values.entreprise}
                  />
                </div>
              </div>
              <textarea
                name="description"
                className={` ${
                  formik.touched.description && formik.errors.description
                    ? "border-2 border-red-600"
                    : null
                } outline-none w-full mt-5 min-h-[150px] max-h-[150px] lg:max-h-[253px] bg-input pt-4 pl-3 placeholder:text-sm`}
                placeholder="Veillez founir des détails de l'objet de la consultation et vos besion *"
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                value={formik.values.description}
              />
              <button
                type="submit"
                className="w-full bg-primary text-white py-4 mt-5"
              >
                SOUMETTRE
              </button>
            </form>
          </div>
          <div className=" w-full  px-5 lg:px-[100px] 2xl:px-[266px]">
            <hr className=" w-full bg-primary mt-9 lg:mt-[74px]" />
            <div className=" grid grid-cols-2 lg:grid-cols-4 lg:gap-[100px] 2xl:gap-[200px]">
              {[
                { label: "Téléphone", value: "418 914 1614" },
                { label: "Cellulaire", value: "581 999 3309" },
                { label: "Fax", value: "418 914 6171" },
                { label: "Email", value: "bgdjiamo@djiamoavocat.ca" },
              ].map((item, i) => {
                return (
                  <div
                    key={i}
                    className="mt-5 w-full flex flex-col items-center justify-center"
                  >
                    <p className=" text-primary text-sm md:text-base ">
                      {item.label}
                    </p>
                    <p className="mt-[10px] text-white text-sm md:text-base">
                      {item.value}
                    </p>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FormCard;
