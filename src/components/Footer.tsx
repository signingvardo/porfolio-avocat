import React from "react";

const Footer = () => {
  return (
    <div className=" bg-[#262626] h-[245px] flex items-center flex-col justify-center">
      <p className=" text-primary ">Adresse</p>
      <p className=" text-white text-center text-xs md:text-base">
        600 Avenue Bévédère, bureau 212, Québec (QC), G1S 3E5
      </p>
      <div className=" mt-14 flex items-center justify-between">
        <img
          className=" cursor-pointer"
          src="/assets/Facebook Icon.svg"
          alt=""
        />
        <img
          src="/assets/LinkedIn Icon.svg"
          className="ml-5 cursor-pointer"
          alt=""
        />
      </div>
    </div>
  );
};

export default Footer;
