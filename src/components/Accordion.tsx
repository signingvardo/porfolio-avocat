import React, { useState } from "react";
interface AccordionProps {
  TypeDroit: any;
}
const Accordion = ({ TypeDroit }: AccordionProps) => {
  const [open, setOpen] = useState(false);

  return (
    <div>
      <div
        key={TypeDroit.droit}
        className=" cursor-pointer w-full font-bold px-5 h-20 flex items-center justify-between border-2"
        onClick={() => setOpen(!open)}
      >
        <div className=" flex items-center gap-x-[30px]">
          <img src={TypeDroit.icon} alt="" />
          <p>{TypeDroit.droit}</p>
        </div>
        <img
          src="/assets/Chevron.svg"
          className={`${open ? "rotate-180 transition-all" : "transition-all"}`}
          alt=""
        />
      </div>
      {open && (
        <div className="bg-[#FAF9F6] flex flex-col gap-5 h-[450px] pt-[35px] pl-8">
          {[
            "Analyse et rédaction de contrats divers",
            " Analyse et rédaction de promesse d’achat et des contrats ",
            "Action en passation de titre ",
            "Représentation devant les tribunaux et devant la Régie du logement ",
            "Perception de comptes",
            "Avis de dénonciation en matière de vices cachés",
            "Enregistrement d’hypothèque légale de la construction",
            "Préavis d’exercice de recours",
          ].map((item) => (
            <div key={item} className=" flex gap-2">
              <img src="/assets/arrow-right-outline.svg" alt="" />
              <p className=" text-sm md:text-base">{item}</p>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default Accordion;
