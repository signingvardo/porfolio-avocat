import React, { useRef } from "react";
import Head from "./components/Head";
import AvacatCard from "./components/AvacatCard";
import FormCard from "./components/FormCard";
import Footer from "./components/Footer";

const Home = () => {
  const ref1 = useRef<HTMLDivElement>(null);
  const ref2 = useRef<HTMLDivElement>(null);
  const ref3 = useRef<HTMLDivElement>(null);

  return (
    <div className="">
      <Head ref1={ref1} ref2={ref2} ref3={ref3} />
      <AvacatCard ref1={ref1} ref2={ref2} ref3={ref3} />
      <FormCard ref3={ref3} />
      <Footer />
    </div>
  );
};

export default Home;
