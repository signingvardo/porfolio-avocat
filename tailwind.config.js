/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      backgroundImage: {
        bg1: "url('/public/assets/bg1')",
        bg2: "url('/public/assets/bg2')",
        bg3: "url('/public/assets/avocat')",
      },
      colors: {
        primary: "#B1976B",
        secondary: "#8E8E8E",
        brun: "#262626",
        input: "#F5F5F5",
      },
      fontFamily: {
        bresley: ["Bresley", "sans-serif"],
        "segoe-ui": ["Segoe UI", "sans-serif"],
      },
    },
  },
  plugins: [],
};
